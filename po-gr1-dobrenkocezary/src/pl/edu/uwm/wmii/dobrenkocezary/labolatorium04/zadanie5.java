package pl.edu.uwm.wmii.dobrenkocezary.labolatorium04;

import java.util.*;

public class zadanie5 {

    public static float kapital(float k, float p, int n)
    {
        float ka = k;
        p = p/100;
        for(int i=0; i<n;i++)
        {
            float temp = ka*p;
            ka+=temp;
        }
        return ka;
    }

    public static void main(String[] args) {
        Scanner InputValue = new Scanner(System.in);
        float k = InputValue.nextFloat();
        float p = InputValue.nextFloat();
        int n = InputValue.nextInt();

        System.out.print("Kapital na koniec: ");
        System.out.format("%.2f%n", kapital(k,p,n));
    }
}
