package pl.edu.uwm.wmii.dobrenkocezary.labolatorium04;

import java.util.*;

public class zadanie1e {

    public static int[] where(String str, String subStr) {
        int r =(str.split(subStr,-1).length)-1;
        int wynik[]=new int[r];
        int licznik=0;
        int i =0;
        while(i != -1) {
            i =str.indexOf(subStr,i);
            if(i != -1) {
                wynik[licznik]=i;
                i+=subStr.length();
                licznik++;
            }
        }
        return wynik;
    }
    public static void wyswietl(int tab[]) {
        for(int i=0;i<tab.length;i++) System.out.print(tab[i]+" , ");
    }

    public static void main(String[] args) {
        String str1;
        String str2;
        int indeksy[];
        Scanner InputValue=new Scanner(System.in);
        System.out.print("NAPIS1 : ");
        str1=InputValue.nextLine();
        System.out.print("SUBSTRING : ");
        str2=InputValue.nextLine();
        indeksy=where(str1,str2);
        System.out.print("WYNIK : ");
        wyswietl(indeksy);
    }
}
