package pl.edu.uwm.wmii.dobrenkocezary.labolatorium04;

import java.util.*;
import java.io.File;
import java.io.FileNotFoundException;

public class zadanie2 {

    public static int plik(String str, char ch) throws FileNotFoundException
    {
        int ile=0;
        Scanner in = new Scanner(new File(str));
        String s = in.nextLine();
        for(int i = 0; i < s.length()+1; i++) if(s.charAt(i) == ch) ile++;
        return ile;
    }

    public static void main(String[] args) throws FileNotFoundException {
        Scanner InputValue = new Scanner(System.in);
        String str = InputValue.nextLine();
        char ch = InputValue.next().charAt(0);
        System.out.println("Ilość wystapień: "+ ch + ": " + plik(str,ch));
        plik(str,ch);
    }
}
