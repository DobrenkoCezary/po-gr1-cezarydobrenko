package pl.edu.uwm.wmii.dobrenkocezary.labolatorium04;

import java.util.*;

public class zadanie1f {

    public static String change(String str){
        StringBuffer sb =new StringBuffer();
        String wynik;
        for(int i=0;i<str.length();i++) {
            char tmp=str.charAt(i);
            if(Character.isLowerCase(tmp)) sb.append(Character.toUpperCase(tmp));
            else sb.append(Character.toLowerCase(tmp));
        }
        wynik = sb.toString();
        return wynik;
    }

    public static void main(String[] args) {
        Scanner InputValue = new Scanner(System.in);
        String str = InputValue.nextLine();
        System.out.println(change(str));
    }
}
