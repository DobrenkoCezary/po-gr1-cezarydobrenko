package pl.edu.uwm.wmii.dobrenkocezary.labolatorium04;

import java.util.*;

public class zadanie1a {

    public static int countChar(String str, char c){
        int licznik = 0;
        for(int i=0; i<str.length(); i++) if(str.charAt(i)==c) licznik++;
        return licznik;
    }

    public static void main(String[] args) {
        Scanner InputValue = new Scanner(System.in);
        String string1 = InputValue.nextLine();
        char litera = InputValue.next().charAt(0);
        System.out.println("Litera: "+ litera +" jest "+ countChar(string1, litera) +" razy.");
    }
}
