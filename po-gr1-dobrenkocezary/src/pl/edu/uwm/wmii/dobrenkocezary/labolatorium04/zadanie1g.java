package pl.edu.uwm.wmii.dobrenkocezary.labolatorium04;

import java.util.*;

public class zadanie1g {

    public static String nice(String str) {
        StringBuffer reversed = new StringBuffer(str);
        reversed.reverse();
        StringBuffer result = new StringBuffer();
        for (int i = 1, j = 0; j < reversed.length(); i++) {
            if (i % 4 == 0)
                result.append('\'');
            else {
                result.append(reversed.charAt(j));
                j++;
            }
        }
        result.reverse();
        return result.toString();
    }

    public static void main(String[] args) {
        Scanner InputValue = new Scanner(System.in);
        String str = InputValue.nextLine();
        System.out.println(nice(str));
    }
}
