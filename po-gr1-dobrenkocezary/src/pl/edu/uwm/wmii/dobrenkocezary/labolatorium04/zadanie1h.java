package pl.edu.uwm.wmii.dobrenkocezary.labolatorium04;

import java.util.Scanner;

public class zadanie1h {

    public static String nice(String str, char sep, int pos) {
        StringBuffer reversed = new StringBuffer(str);
        reversed.reverse();
        StringBuffer result = new StringBuffer();
        for (int i = 1, j = 0; j < reversed.length(); i++) {
            if (i % (pos + 1) == 0)
                result.append(sep);
            else {
                result.append(reversed.charAt(j));
                j++;
            }
        }
        result.reverse();
        return result.toString();
    }

    public static void main(String[] args) {
        Scanner InputValue = new Scanner(System.in);
        String str = InputValue.nextLine();
        char zm1 = 'c';
        int zm2 = 4;
        System.out.println(nice(str, zm1, zm2));
    }
}
