package pl.edu.uwm.wmii.dobrenkocezary.labolatorium04;

import java.util.*;

public class zadanie1b {

    public static int countSubStr(String str, String SubStr){
        int ilosc = 0;
        for(int i=0; i<str.length()-SubStr.length()+1;i++) if(str.substring(i,i+SubStr.length()).equals(SubStr)) ilosc++;
        return ilosc;
    }

    public static void main(String[] args) {
        Scanner InputValue = new Scanner(System.in);
        String str1 = InputValue.nextLine();
        String str2 = InputValue.nextLine();
        System.out.println("Ilosc wystapien: "+countSubStr(str1,str2));
    }
}
