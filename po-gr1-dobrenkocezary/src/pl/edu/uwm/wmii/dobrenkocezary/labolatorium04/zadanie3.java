package pl.edu.uwm.wmii.dobrenkocezary.labolatorium04;

import java.util.*;
import java.io.File;
import java.io.FileNotFoundException;


public class zadanie3 {

    public static int plik(String str, String ch) throws FileNotFoundException
    {
        int ile=0;
        Scanner in = new Scanner(new File(str));
        String zdanie = in.nextLine();
        for(int i=0; i<zdanie.length()-ch.length()+1;i++) if(zdanie.substring(i, i+ch.length()).equals(ch)) ile++;
        return ile;
    }

    public static void main(String[] args) throws FileNotFoundException {
        Scanner klawiatura = new Scanner(System.in);
        String nazwa = klawiatura.nextLine();
        String wyraz = klawiatura.nextLine();
        System.out.println("Ilosc wystapien: "+plik(nazwa,wyraz));
    }

}
