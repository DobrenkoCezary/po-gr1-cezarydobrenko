package pl.edu.uwm.wmii.dobrenkocezary.labolatorium04;

import java.util.*;

public class zadanie1d {

    public static String repeat(String str, int n){
        String napis = "";
        for(int j=0; j<n; j++) for(int i=0; i<str.length(); i++) napis += str.charAt(i);
        return napis;
    }

    public static void main(String[] args) {
        Scanner InputValue = new Scanner(System.in);
        String napis = InputValue.nextLine();
        int n = InputValue.nextInt();
        System.out.println("Wynik: "+repeat(napis, n));
    }
}
