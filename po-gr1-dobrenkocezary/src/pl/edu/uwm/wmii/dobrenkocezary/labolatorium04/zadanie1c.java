package pl.edu.uwm.wmii.dobrenkocezary.labolatorium04;

import java.util.*;

public class zadanie1c {

    public static String middle(String str){
        String napis = "";
        for(int i=0; i<str.length(); i++) {
            if(str.length()%2==0 & (i==str.length()/2 || i==str.length()/2-1)) napis += str.charAt(i);
            else if(str.length()%2!=0 & i==str.length()/2) napis += str.charAt(i);
        }
        return napis;
    }

    public static void main(String[] args) {
            Scanner InputValue = new Scanner(System.in);
        String napis = InputValue.nextLine();
        System.out.println("Srodkowy: "+middle(napis));
    }

}
