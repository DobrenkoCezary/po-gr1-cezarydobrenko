package pl.edu.uwm.wmii.dobrenkocezary.labolatorium04;

import java.util.*;

public class zadanie4 {

    public static int SzachownicaIlosc(int n)
    {
        int a = 1;
        for(int i=1; i<n*n; i++) {
            a *= 2;
        }
        return a;
    }
    public static void main(String[] args) {
        Scanner InputValue = new Scanner(System.in);
        int n = InputValue.nextInt();
        System.out.println("Ilosc ziaren: "+SzachownicaIlosc(n));
    }
}
