package pl.edu.uwm.wmii.dobrenkocezary.labolatorium08;

import java.util.Arrays;


public class TestOsoba
{
    public static void main(String[] args)
    {
        Osoba[] ludzie = new Osoba[2];

        ludzie[0] = new Pracownik("Bonzo", "Haker", "Kobra", 1999, 11, 22, 2011, 2, 3, false, 4200.25);
        ludzie[1] = new Student("Hajzer", "Zygmunt", 1998, 02, 11, true, "Informatyka", 5.0);

        for (Osoba p : ludzie) {
            System.out.println(p.getNazwisko() + " "+ Arrays.deepToString(p.getimiona()) + " : " + p.getOpis() + " ,Data urodzenia: " + p.getdataUrodzenia() + " ,Płeć: " + p.getpłeć());
        }
    }
}


