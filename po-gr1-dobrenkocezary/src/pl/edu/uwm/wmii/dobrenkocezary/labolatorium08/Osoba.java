package pl.edu.uwm.wmii.dobrenkocezary.labolatorium08;

import java.time.LocalDate;

public abstract class Osoba
{
    public Osoba(String nazwisko, String imię, String drugieImię, int rokUr, int miesiacUr, int dzienUr, boolean płeć)
    {
        String[] imiona = {imię, drugieImię};
        this.nazwisko = nazwisko;
        this.imiona = imiona;
        this.dataUrodzenia = LocalDate.of(rokUr,miesiacUr,dzienUr);
        this.płeć = płeć;
    }

    public Osoba(String nazwisko,String imię, int rokUr, int miesiacUr, int dzienUr, boolean płeć)
    {
        String[] imiona = {imię};

        this.nazwisko = nazwisko;
        this.imiona = imiona;
        this.dataUrodzenia = LocalDate.of(rokUr,miesiacUr,dzienUr);
        this.płeć = płeć;
    }


    public abstract String getOpis();

    public String getNazwisko() { return nazwisko; }
    public String[] getimiona(){ return imiona;}
    public LocalDate getdataUrodzenia(){return dataUrodzenia;}
    public boolean getpłeć(){ return płeć;}


    private String nazwisko;
    public String[] imiona;
    public LocalDate dataUrodzenia;
    public boolean płeć;
}
