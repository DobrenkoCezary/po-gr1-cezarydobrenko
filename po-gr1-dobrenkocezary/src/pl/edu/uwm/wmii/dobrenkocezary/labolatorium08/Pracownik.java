package pl.edu.uwm.wmii.dobrenkocezary.labolatorium08;

import java.time.LocalDate;

public class Pracownik extends Osoba
{
    public Pracownik(String nazwisko, String imię, String drugieImię, int rokU, int miesiacU, int dzienU, int rokZ, int miesiacZ, int dzienZ, boolean płeć, double pobory)
    {
        super( nazwisko, imię, drugieImię, rokU, miesiacU, dzienU, płeć);
        this.pobory = pobory;
        this.dataZatrudnienia = LocalDate.of(rokZ, miesiacZ, dzienZ);
    }
    public Pracownik(String nazwisko, String imię, int rokU, int miesiacU, int dzienU, int rokZ, int miesiacZ, int dzienZ, boolean płeć, double pobory)
    {
        super( nazwisko, imię, rokU, miesiacU, dzienU, płeć);
        this.pobory = pobory;

        this.dataZatrudnienia = LocalDate.of(rokZ, miesiacZ, dzienZ);
    }

    public LocalDate getdataZatrudnienia(){return dataZatrudnienia;}
    public double getPobory() { return pobory; }
    public String getOpis()
    {
        return String.format("pracownik z pensją %.2f zł", pobory);
    }

    private double pobory;
    public LocalDate dataZatrudnienia;

}
