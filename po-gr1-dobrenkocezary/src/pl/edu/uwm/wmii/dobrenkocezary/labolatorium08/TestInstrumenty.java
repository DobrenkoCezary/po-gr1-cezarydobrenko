package pl.edu.uwm.wmii.dobrenkocezary.labolatorium08;

import pl.imiajd.dobrenko.Flet;
import pl.imiajd.dobrenko.Fortepian;
import pl.imiajd.dobrenko.Instrument;
import pl.imiajd.dobrenko.Skrzypce;

import java.time.LocalDate;
import java.util.ArrayList;

public class TestInstrumenty {
    public static void main(String[] args) {
        ArrayList<Instrument> orkiestra = new ArrayList<>();
        orkiestra.add(new Skrzypce("SKRZYPCECAMPANY", LocalDate.of(2015, 5, 15)));
        orkiestra.add(new Flet("FLETCAMPANY", LocalDate.of(2011, 1, 11)));
        orkiestra.add(new Fortepian("FORTEPIANCAMPANY", LocalDate.of(2012, 2, 12)));
        orkiestra.add(new Skrzypce("SKRZYPCECAMPANY", LocalDate.of(2013, 3, 13)));
        orkiestra.add(new Flet("FLETCAMPANY", LocalDate.of(2014, 4, 14)));
        for (Instrument i : orkiestra)
            System.out.println(i.dzwiek());
        System.out.println(orkiestra.toString());
    }
}
