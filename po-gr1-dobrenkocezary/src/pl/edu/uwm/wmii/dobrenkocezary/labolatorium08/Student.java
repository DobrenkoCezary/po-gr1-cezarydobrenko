package pl.edu.uwm.wmii.dobrenkocezary.labolatorium08;

import java.time.LocalDate;

public class Student extends Osoba
{
    public Student(String nazwisko, String imię, String drugieImię, int rokUr, int miesiacUr, int dzienUr, boolean płeć, String kierunek, double średniaOcen)
    {
        super(nazwisko, imię, drugieImię, rokUr, miesiacUr, dzienUr, płeć);
        this.kierunek = kierunek;
        this.średniaOcen = średniaOcen;

    }

    public Student(String nazwisko, String imię, int rokUr, int miesiacUr, int dzienUr, boolean płeć, String kierunek, double średniaOcen)
    {
        super(nazwisko, imię, rokUr, miesiacUr, dzienUr, płeć);
        this.kierunek = kierunek;
        this.średniaOcen = średniaOcen;

    }

    public String getOpis()
    {
        return "kierunek studiów: " + kierunek;
    }
    public double getśredniaOcen() { return średniaOcen; }
    public void set(double średniaOcen) { this.średniaOcen = średniaOcen; }
    private String kierunek;
    public LocalDate dataZatrudnienia;
    public double średniaOcen;
}
