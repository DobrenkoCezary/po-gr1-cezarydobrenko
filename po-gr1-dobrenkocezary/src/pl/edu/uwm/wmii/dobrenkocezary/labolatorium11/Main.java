package pl.edu.uwm.wmii.dobrenkocezary.labolatorium11;


import java.time.LocalDate;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        //Zadanie 1
        Pair<String> test = new Pair<String>();
        test.setFirst("5");
        test.setSecond("7");
        System.out.println(test.getFirst());
        System.out.println(test.getSecond());
        test.swap();
        System.out.println(test.getFirst());
        System.out.println(test.getSecond());

        //Zadanie 2
        PairUtil<String> test1 = new PairUtil<String>();
        test = test1.swap(test);
        System.out.println(test.getFirst());
        System.out.println(test.getSecond());

        //Zadanie 3
        Item[] Unsorteditems = new Item[4];
        Unsorteditems[0] = new Item(100);
        Unsorteditems[1] = new Item(0);
        Unsorteditems[2] = new Item(200);
        Unsorteditems[3] = new Item(50);

        Arrays.sort(Unsorteditems);

        for (Item element : Unsorteditems) System.out.println(element);

        Item[] Sorteditems = new Item[4];
        Sorteditems[0] = new Item(0);
        Sorteditems[1] = new Item(50);
        Sorteditems[2] = new Item(100);
        Sorteditems[3] = new Item(200);

        Arrays.sort(Sorteditems);

        for (Item element : Sorteditems) System.out.println(element);
        
    }
}