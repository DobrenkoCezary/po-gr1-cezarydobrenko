package pl.edu.uwm.wmii.dobrenkocezary.labolatorium11;


class Item<T> implements Comparable<Item<T>> {

    public Item(T value){
        this.value = value;
    }

    public int compareTo(Item other)
    {
        int x = (int) this.value;
        int y = (int) other.value;
        if(x < y) return -1;
        if(x > y) return 1;
        return 0;
    }

    public String toString(){
       return value.toString();
    }

    public T value;
}

