package pl.edu.uwm.wmii.dobrenkocezary.labolatorium06;

import java.time.Instant;
import java.time.LocalDate;
import java.util.Date;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

public class zadanie3 {
    public static void main(String[] args)
    {
        Pracownik[] personel = new Pracownik[3];

        // wypełnij tablicę danymi pracowników
        personel[0] = new Pracownik("Karol Cracker", 75000);
        personel[1] = new Pracownik("Henryk Hacker", 50000);
        personel[2] = new Pracownik("Antoni Tester", 40000);

        // zwieksz pobory każdego pracownika o 20%
        for (Pracownik e : personel) {
            e.zwiekszPobory(20);
        }

        // wypisz informacje o każdym pracowniku
        for (Pracownik e : personel) {
            System.out.print("nazwisko = " + e.nazwisko() + "\tid = " + e.id());
            System.out.print("\tpobory = " + e.pobory());
            System.out.printf("\tdataZatrudnienia = %tF\n", e.dataZatrudnienia());
        }
        System.out.println();

        int n = Pracownik.getNextId(); // wywołanie metody statycznej
        System.out.println("Następny dostępny id = " + n);

    }
}

class Pracownik
{
    public Pracownik(String nazwisko, double pobory)
    {
        this.nazwisko = nazwisko;
        this.pobory = pobory;


        Date date = new Date();
        Instant instant = date.toInstant();
        ZoneId defaultZoneId = ZoneId.systemDefault();

        LocalDate localDate = instant.atZone(defaultZoneId).toLocalDate();
        dataZatrudnienia = localDate;


        id = nextId;
        ++nextId;
    }

    public String nazwisko()
    {
        return nazwisko;
    }

    public double pobory()
    {
        return pobory;
    }

    public LocalDate  dataZatrudnienia()
    {
        return dataZatrudnienia;
    }

    public void zwiekszPobory(double procent)
    {
        double podwyżka = pobory * procent / 100;
        pobory += podwyżka;
    }

    public int id()
    {
        return id;
    }

    public void setId()
    {
        id = nextId;
        ++nextId;
    }

    public static int getNextId()
    {
        return nextId;
    }

    private String nazwisko;
    private double pobory;
    private LocalDate dataZatrudnienia;

    private int id;
    private static int nextId = 1;
}
