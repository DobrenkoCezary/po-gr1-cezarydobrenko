package pl.edu.uwm.wmii.dobrenkocezary.labolatorium06;

import java.util.*;

public class RachunekBankowy {
    static public double rocznaStopaProcentowa = 0;

    private double saldo;

    public double Saldo(){
        return saldo;
    }

    public void setSaldo(double n){
        saldo = n;
    }

    public double obliczMiesieczneOdsetki(){
        return (saldo * rocznaStopaProcentowa)/12;
    }

    public double setRocznaStopaProcentowa(double n){
        return rocznaStopaProcentowa=n;
    }
}
