package pl.edu.uwm.wmii.dobrenkocezary.labolatorium06;

import java.util.*;

public class zadanie1 {
    public static void main(String[] args) {
        RachunekBankowy saver1 = new RachunekBankowy();
        RachunekBankowy saver2 = new RachunekBankowy();

        saver1.setSaldo(2000.00);
        saver2.setSaldo(3000.00);
        saver1.setRocznaStopaProcentowa(0.04);
        saver2.setRocznaStopaProcentowa(0.04);


        saver1.setSaldo(saver1.Saldo()+ saver1.obliczMiesieczneOdsetki());
        saver2.setSaldo(saver2.Saldo()+ saver2.obliczMiesieczneOdsetki());

        System.out.println("Saldo saver1 po pierwszym miesiącu z 4% wynosi: " + saver1.Saldo());
        System.out.println("Saldo saver2 po pierwszym miesiącu z 4% wynosi: " + saver2.Saldo());

        saver1.setRocznaStopaProcentowa(0.05);
        saver2.setRocznaStopaProcentowa(0.05);

        saver1.setSaldo(saver1.Saldo()+ saver1.obliczMiesieczneOdsetki());
        saver2.setSaldo(saver2.Saldo()+ saver2.obliczMiesieczneOdsetki());

        System.out.println("Saldo saver1 po drugim miesiącu z 5% wynosi: " + saver1.Saldo());
        System.out.println("Saldo saver2 po drugim miesiącu z 5% wynosi: " + saver2.Saldo());
    }
}
