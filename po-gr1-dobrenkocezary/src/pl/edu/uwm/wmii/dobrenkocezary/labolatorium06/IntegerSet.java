package pl.edu.uwm.wmii.dobrenkocezary.labolatorium06;

public class IntegerSet {

    boolean[] tab = new boolean[99];

    public IntegerSet(){
        for(int i = 0; i < tab.length; i++) tab[i] = false;
    }


    public static boolean[] union(boolean[] T1, boolean[] T2){
        int rozmiar;
        boolean[] NEW = new boolean[99];
        if(T1.length > T2.length) rozmiar = T1.length;
        else rozmiar = T2.length;
        for(int i = 0; i < rozmiar; i++) if((T1[i] == true) || (T2[i] == true)) NEW[i] = true;
        return NEW;
    }

    public static boolean[] intersection(boolean[] T1, boolean[] T2){
        boolean[] NEW = new boolean[99];
        int rozmiar;
        if(T1.length > T2.length) rozmiar = T1.length;
        else rozmiar = T2.length;
        for(int i = 0; i < rozmiar; i++) if((T1[i] == true) && (T2[i] == true)) NEW[i] = true;
        return NEW;
    }


    public void insertElement(int n){tab[n-1] = true;}
    public void deleteElement(int n){tab[n-1] = false;}


    public String toString(){
        String str = "";
        for(int i = 0; i < tab.length; i++){
            if(tab[i] == true){
                str+= Integer.toString(i+1);
                str+= " ";
            }
        }
        return str;
    }
    public void Equals(IntegerSet u1){
        int l = 0;
        for(int i = 0; i < tab.length; i++) if(tab[i] != u1.tab[i]) l++;
        if(l==0) System.out.println("Zbiory są identyczne.");
        else System.out.println("Zbiory nie są identyczne.");
    }
}
