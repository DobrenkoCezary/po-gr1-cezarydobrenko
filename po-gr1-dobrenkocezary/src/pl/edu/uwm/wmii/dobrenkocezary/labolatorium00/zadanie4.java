package pl.edu.uwm.wmii.dobrenkocezary.labolatorium00;

public class zadanie4 {

    public static void main(String[] args) {
        int  saldo = 1000;
        System.out.println("saldo początkowe: " + saldo);
        saldo *= 1.06;
        System.out.println("saldo po 1 roku: " + saldo);
        saldo *= 1.06;
        System.out.println("saldo po 2 roku: " + saldo);
        saldo *= 1.06;
        System.out.println("saldo po 3 roku: " + saldo);
    }
}
