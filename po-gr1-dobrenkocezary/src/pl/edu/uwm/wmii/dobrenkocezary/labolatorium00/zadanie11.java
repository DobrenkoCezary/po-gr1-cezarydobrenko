package pl.edu.uwm.wmii.dobrenkocezary.labolatorium00;

public class zadanie11 {

    public static void main(String[] args) {

        System.out.println("Mróz\n" +
                "W ostry mróz chłopek wiózł\n" +
                "Z lasu chrust na wozie,\n" +
                "Skrzypi coś, oś nie oś,\n" +
                "Trzaska chrust na mrozie.\n" +
                "\n" +
                "Tężał mróz, wicher rósł,\n" +
                "Pędząc jak w sto koni,\n" +
                "Trzeszczy wóz, trzeszczy mróz,\n" +
                "Chłop zębami dzwoni.");
    }
}
