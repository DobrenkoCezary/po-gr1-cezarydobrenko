package pl.edu.uwm.wmii.dobrenkocezary.labolatorium01;
import java.util.*;
import static java.lang.Math.abs;

public class zadanie1_1c {

    public static void main(String[] args) {
        Scanner InputValue = new Scanner(System.in);
        System.out.println("Podaj ile liczb do sumy: ");
        int n = InputValue.nextInt();
        int suma = 0;

        for(int i = 0; i < n; i++)
        {
            System.out.println("Podaj następną wartość: ");
            int a = InputValue.nextInt();
            suma += abs(a);
        }
        System.out.print(suma);
    }
}
