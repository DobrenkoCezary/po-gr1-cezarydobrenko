package pl.edu.uwm.wmii.dobrenkocezary.labolatorium01;

import java.util.Scanner;

public class zadanie1_2 {

    public static void main(String[] args) {
        Scanner InputValue = new Scanner(System.in);
        System.out.println("Podaj ile liczb: ");
        int n = InputValue.nextInt();
        float tmp = 0;

        for(int i = 0; i < n; i++) {
            System.out.println("Podaj następną wartość: ");
            float a = InputValue.nextFloat();
            if(i == 0) tmp = a;
            else System.out.println(a);
        }
        System.out.println(tmp);
    }
}
