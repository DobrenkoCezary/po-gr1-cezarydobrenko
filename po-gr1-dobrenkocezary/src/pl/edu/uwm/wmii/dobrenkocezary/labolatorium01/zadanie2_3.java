package pl.edu.uwm.wmii.dobrenkocezary.labolatorium01;

import java.util.*;

public class zadanie2_3 {

    public static void main(String[] args) {
        Scanner InputValue = new Scanner(System.in);
        int n = InputValue.nextInt();
        int ujemnych = 0;
        int dodatnich = 0;
        int zer = 0;

        for (int i = 1; i <= n; i++) {
            float a = InputValue.nextFloat();
            if(a<0) ujemnych += 1;
            if(a>0) dodatnich +=1;
            if(a==0) zer += 1;
        }
        System.out.println("Dodatnich: "+dodatnich);
        System.out.println("Ujemnych: "+ujemnych);
        System.out.println("Zer: "+zer);

    }
}
