package pl.edu.uwm.wmii.dobrenkocezary.labolatorium01;

import java.util.*;

public class zadanie2_2 {

    public static void main(String[] args) {
        Scanner InputValue = new Scanner(System.in);
        int n = InputValue.nextInt();
        float wynik = 0;

        for (int i = 1; i <= n; i++) {
            float a = InputValue.nextFloat();
            if(a>0) wynik += a;
        }
        wynik*=2;
        System.out.println(wynik);

    }
}
