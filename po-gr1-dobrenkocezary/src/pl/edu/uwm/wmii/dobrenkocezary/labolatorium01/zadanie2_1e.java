package pl.edu.uwm.wmii.dobrenkocezary.labolatorium01;

import java.util.*;
import static java.lang.Math.pow;

public class zadanie2_1e {

    public static void main(String[] args) {
        Scanner InputValue = new Scanner(System.in);
        int n1 = InputValue.nextInt();
        int n2 = InputValue.nextInt();
        int wynik = 0;

        if ((1<=n2) & (n1>=1) & (n2<=n1))
        {
            for(int i = 1; i <= n2; i++)
            {
                int a = InputValue.nextInt();
                if (pow(2,i) < a & a < silnia(i))
                    wynik++;
            }
        }
        System.out.println(wynik);
    }

    public static int silnia (int n) {
        int iloczyn = 1;
        for (int i=1; i<=n; i++) {
            iloczyn *= i;
        }
        return iloczyn;
    }
}
