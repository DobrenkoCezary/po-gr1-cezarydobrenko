package pl.edu.uwm.wmii.dobrenkocezary.labolatorium01;

import java.util.Scanner;

public class zadanie2_1c {

    public static void main(String[] args) {
        Scanner InputValue = new Scanner(System.in);
        System.out.println("Podaj ile liczb do sumy: ");
        int n = InputValue.nextInt();
        int suma = 0;

        for (int i=0; i<n;i++) {
            System.out.println("Podaj następną wartość: ");
            int a = InputValue.nextInt();
            for (int j=0; j<a;j++) {
                if((j%2 == 0) && (a == (j*j))) suma++;
            }
        }
        System.out.print("kwadraty liczb parzystych: "+suma);
    }
}
