package pl.edu.uwm.wmii.dobrenkocezary.labolatorium01;

import java.util.Scanner;

public class zadanie2_1d {

    public static void main(String[] args) {
        Scanner InputValue = new Scanner(System.in);
        int n1 = InputValue.nextInt();
        int n2 = InputValue.nextInt();
        int tmp;
        int wynik = 0;

        if (1 < n2 & n2 < n1 & n1 > 2) {
            int a1 = InputValue.nextInt();
            int a2 = InputValue.nextInt();
            for (int i = 3; i <= n2; i++) {
                tmp = InputValue.nextInt();
                if (a2 < (a1 + tmp) / 2)
                    wynik++;
                a1 = a2;
                a2 = tmp;
            }
            System.out.println(wynik);
        }
    }
}
