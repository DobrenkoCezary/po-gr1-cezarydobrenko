package pl.edu.uwm.wmii.dobrenkocezary.labolatorium01;

import java.util.Scanner;

public class zadanie1_1g {

    public static void main(String[] args) {
        Scanner InputValue = new Scanner(System.in);
        System.out.println("Podaj ile liczb do sumy i ilorazu: ");
        int n = InputValue.nextInt();
        int suma = 0;
        int iloraz = 1;

        for(int i = 0; i < n; i++) {
            System.out.println("Podaj następną wartość: ");
            int a = InputValue.nextInt();
            suma += a;
            iloraz *= a;
        }
        System.out.println("suma: "+suma);
        System.out.println("iloraz: "+iloraz);
    }
}
