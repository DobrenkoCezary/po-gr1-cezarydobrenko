package pl.edu.uwm.wmii.dobrenkocezary.labolatorium01;

import java.util.*;

public class zadanie2_4 {

    public static void main(String[] args) {
        Scanner InputValue = new Scanner(System.in);
        int n = InputValue.nextInt();
        float najmniejsza = 0;
        float najwieksza = 0;

        for (int i = 1; i <= n; i++) {
            float a = InputValue.nextFloat();
            if(najmniejsza > a) najmniejsza = a;
            if(najwieksza < a) najwieksza = a;
        }
        System.out.println("Najmniejsza: "+najmniejsza);
        System.out.println("Najwieksza: "+najwieksza);
    }
}
