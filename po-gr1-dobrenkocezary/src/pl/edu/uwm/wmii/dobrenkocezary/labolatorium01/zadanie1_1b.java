package pl.edu.uwm.wmii.dobrenkocezary.labolatorium01;

import java.util.Scanner;

public class zadanie1_1b {

    public static void main(String[] args) {
        Scanner InputValue = new Scanner(System.in);
        System.out.println("Podaj ile liczb do ilorazu: ");
        int n = InputValue.nextInt();
        int iloraz = 1;

        for(int i = 0; i < n; i++) {
            System.out.println("Podaj następną wartość: ");
            int a = InputValue.nextInt();
            iloraz *= a;
        }
        System.out.print(iloraz);
    }
}
