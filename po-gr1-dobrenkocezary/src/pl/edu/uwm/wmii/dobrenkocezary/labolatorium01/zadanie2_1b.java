package pl.edu.uwm.wmii.dobrenkocezary.labolatorium01;

import java.util.Scanner;

public class zadanie2_1b {

    public static void main(String[] args) {
        Scanner InputValue = new Scanner(System.in);
        System.out.println("Podaj ile liczb do sumy: ");
        int n = InputValue.nextInt();
        int suma = 0;

        for(int i = 0; i < n; i++) {
            System.out.println("Podaj następną wartość: ");
            int a = InputValue.nextInt();
            if((a%3==0) &&(a%5==0)) suma++;
        }
        System.out.print("liczb podzielnych przez 3 i 5: "+suma);
    }
}
