package pl.edu.uwm.wmii.dobrenkocezary.labolatorium10;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.io.*;

public class Main {

    public static void main(String[] args) throws Exception  {
        Osoba obj1 = new Osoba("Bonzo",1997,11,12);
        Osoba obj3 = new Osoba("Bonzo",1995,11,12);
        Osoba obj2 = new Osoba("Kobra",1995,01,22);
        System.out.println(obj1.toString());
        System.out.println(obj2.toString());
        System.out.println(obj1.equals(obj1));
        System.out.println(obj1.equals(obj2));
        System.out.println(obj1.compareTo(obj1));
        System.out.println(obj1.compareTo(obj2));
        System.out.println(obj1.compareTo(obj3));

        List<Osoba> lista = new ArrayList<Osoba>();
        Osoba l5 = new Osoba("Dobrenko",1997,9,9);
        Osoba l1 = new Osoba("AAA",1998,11,12);
        Osoba l3 = new Osoba("CCC",1992,11,12);
        Osoba l4 = new Osoba("DDD",1992,11,12);
        Osoba l2 = new Osoba("AAA",1997,11,12);
        Osoba l6 = new Osoba("AAjA",1996,11,12);
        Osoba l7 = new Osoba("AAA",1992,2,12);
        Osoba l8 = new Osoba("AAA",1992,3,12);
        Osoba l9 = new Osoba("AAA",1992,2,2);
        Osoba l10 = new Osoba("AAA",1992,3,1);

        Osoba s1 = new Student("AAA",1992,3,1,4.33);
        Osoba s2 = new Student("AAA",1992,3,2,4.00);
        Osoba s3 = new Student("AAA",1992,4,1,4.00);
        Osoba s4 = new Student("AAA",1993,3,1,4.00);
        Osoba s5 = new Student("AAB",1992,3,2,4.00);
        Osoba s6 = new Student("AAA",1992,3,2,4.11);
        Osoba s7 = new Student("AAA",1992,3,2,4.01);
        /*
        lista.add(l5);
        lista.add(l1);
        lista.add(l3);
        lista.add(l4);
        lista.add(l2);
        lista.add(l6);
        lista.add(l7);
        lista.add(l8);
        lista.add(l9);
        lista.add(l10);
        */
        lista.add(s1);
        lista.add(s2);
        lista.add(s3);
        lista.add(s4);
        lista.add(s5);
        lista.add(s6);
        lista.add(s7);


        for(Osoba o : lista) {
            System.out.println(o.toString());
        }
        System.out.println("-------------------------------");

        Collections.sort(lista);

        for(Osoba o : lista) {
            System.out.println(o.toString());
        }
        System.out.println("-------------------------------");


        // read from file

        File file = new File("C:\\Users\\ABC\\IdeaProjects\\po-gr1-dobrenkocezary\\src\\pl\\edu\\uwm\\wmii\\dobrenkocezary\\labolatorium10\\test.txt");

        BufferedReader br = new BufferedReader(new FileReader(file));
        List<String> Stringi = new ArrayList<String>();

        String st;
        while ((st = br.readLine()) != null)
            Stringi.add(st);

        Collections.sort(Stringi);

        for(String x : Stringi) {
            System.out.println(x);
        }
        System.out.println("-------------------------------");

    }
}