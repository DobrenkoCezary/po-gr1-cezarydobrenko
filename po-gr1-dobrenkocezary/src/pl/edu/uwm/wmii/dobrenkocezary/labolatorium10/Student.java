package pl.edu.uwm.wmii.dobrenkocezary.labolatorium10;
import java.time.LocalDate;

class Student extends Osoba implements Comparable<Osoba>, Cloneable {


    public Student(String Nazwisko, int Rok, int Miesiac, int Dzien, double sredniaOcen){
        super(Nazwisko,Rok,Miesiac,Dzien);
        this.sredniaOcen = sredniaOcen;
    }

    public int compareTo(Osoba other)
    {
        Student otherStudent = (Student) other;
        if((super.compareTo(otherStudent)) == 0){
            if ((this.sredniaOcen == otherStudent.sredniaOcen)) {
                return 0;
            }
            else if(this.sredniaOcen < otherStudent.sredniaOcen) return -1;
            else return 1;
        } else return super.compareTo(otherStudent);
    }


    public double getSredniaOcen(){
        return this.sredniaOcen;
    }

    public String toString() { return this.getClass().getSimpleName() + "[" + getNazwisko() +" , " + getDataUrodzenia() + " , " + this.sredniaOcen + "]"; }

    private double sredniaOcen;

}