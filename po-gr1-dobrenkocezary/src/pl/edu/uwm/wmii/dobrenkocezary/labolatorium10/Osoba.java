package pl.edu.uwm.wmii.dobrenkocezary.labolatorium10;
import java.time.LocalDate;

class Osoba implements Comparable<Osoba>, Cloneable {


    public Osoba(String Nazwisko, int Rok, int Miesiac, int Dzien){
        this.nazwisko = Nazwisko;
        this.dataUrodzenia = LocalDate.of(Rok, Miesiac, Dzien);
    }

    public int compareTo(Osoba other)
    {
        if ((this.nazwisko.compareTo(other.nazwisko)) == 0) {
            if ((this.dataUrodzenia.compareTo(other.dataUrodzenia) == 0)) {
                    return 0;
            }
            else return this.dataUrodzenia.compareTo(other.dataUrodzenia);
        }
        return this.nazwisko.compareTo(other.nazwisko);
    }



    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        Osoba other = (Osoba) obj;
        if (nazwisko == null) { if (other.nazwisko != null) return false; }
        else if (!nazwisko.equals(other.nazwisko)) return false;
        if (dataUrodzenia != other.dataUrodzenia) return false;
        return true;
    }

    public String getNazwisko(){
        return this.nazwisko;
    }

    public LocalDate getDataUrodzenia(){
        return this.dataUrodzenia;
    }


    public String toString() { return this.getClass().getSimpleName() + "[" + this.nazwisko + " , " + this.dataUrodzenia + "]"; }

    private LocalDate dataUrodzenia;
    private String nazwisko;

}