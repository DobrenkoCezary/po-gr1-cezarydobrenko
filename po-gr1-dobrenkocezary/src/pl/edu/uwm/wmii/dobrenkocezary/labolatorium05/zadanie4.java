package pl.edu.uwm.wmii.dobrenkocezary.labolatorium05;

import java.util.ArrayList;

public class zadanie4 {

    public static ArrayList<Integer> reversed(ArrayList<Integer> a){
        ArrayList<Integer> lista = new ArrayList<Integer>();
        for (int i=a.size()-1; i>=0; i--) lista.add(a.get(i));
        return lista;
    }
    public static void main(String[] args) {
        ArrayList<Integer>first=new ArrayList<Integer>();
        first.add(1);
        first.add(2);
        first.add(3);
        first.add(4);
        ArrayList<Integer>secound=new ArrayList<Integer>();
        secound.add(4);
        secound.add(3);
        secound.add(2);
        secound.add(1);
        System.out.println(reversed(first));
    }
}
