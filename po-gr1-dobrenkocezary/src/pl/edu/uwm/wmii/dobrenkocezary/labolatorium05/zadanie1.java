package pl.edu.uwm.wmii.dobrenkocezary.labolatorium05;

import java.util.ArrayList;

public class zadanie1 {

    public static ArrayList<Integer> append(ArrayList<Integer> first, ArrayList<Integer> secound)
    {
        ArrayList<Integer>lista=new ArrayList<Integer>();
        lista.addAll(first);
        lista.addAll(secound);
        return lista;
    }

    public static void main(String[] args) {
        ArrayList<Integer>first=new ArrayList<Integer>();
        first.add(1);
        first.add(2);
        first.add(3);
        first.add(4);
        ArrayList<Integer>secound=new ArrayList<Integer>();
        secound.add(4);
        secound.add(3);
        secound.add(2);
        secound.add(1);
        System.out.println(append(first,secound));
    }
}
