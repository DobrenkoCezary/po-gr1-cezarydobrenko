package pl.edu.uwm.wmii.dobrenkocezary.labolatorium05;

import java.util.ArrayList;

public class zadanie3 {

    public static ArrayList<Integer> mergeSorted(ArrayList<Integer> a, ArrayList<Integer> b) {
        ArrayList<Integer> lista = new ArrayList<Integer>();
        int x = 0, y = 0;
        while (x < a.size() && y < b.size())
        {
            if (a.get(x) > b.get(y)) {
                lista.add(b.get(y++));
            }
            else if (a.get(x) < b.get(y)) {
                lista.add(a.get(x++));
            }
            else {
                lista.add(a.get(x++));
                lista.add(b.get(y++));
            }
        }
        if (x < a.size()) {
            while (x < a.size()) {
                lista.add(a.get(x++));
            }
        }
        else if (y < b.size()) {
            while (y < b.size()) {
                lista.add(b.get(y++));
            }
        }
        return lista;
    }

    public static void main(String[] args) {
        ArrayList<Integer>first=new ArrayList<Integer>();
        first.add(1);
        first.add(2);
        first.add(3);
        first.add(4);
        ArrayList<Integer>secound=new ArrayList<Integer>();
        secound.add(4);
        secound.add(3);
        secound.add(2);
        secound.add(1);
        System.out.println(mergeSorted(first,secound));
    }
}
