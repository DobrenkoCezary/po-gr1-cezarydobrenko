package pl.edu.uwm.wmii.dobrenkocezary.labolatorium07;


import pl.imiajd.dobrenko.Nauczyciel;
import pl.imiajd.dobrenko.Student;

public class zadanie2 {

    public static void main(String[] args) {

        Student Cezary = new Student("Dobreńko", 1997, "Informatyka");
        Nauczyciel Paweł = new Nauczyciel("TEST", 1920, 3555.25);

        System.out.println(Cezary.toString());
        System.out.println(Paweł.toString());

        Cezary.setkierunek("Inżynieria");
        Cezary.setnazwisko("Bonzo");
        Cezary.setRokUrodzenia(1222);

        Paweł.setpensja(2000.24);
        Paweł.setnazwisko("testa");
        Paweł.setRokUrodzenia(1333);

        System.out.println(Cezary.toString());
        System.out.println(Paweł.toString());

    }

}

