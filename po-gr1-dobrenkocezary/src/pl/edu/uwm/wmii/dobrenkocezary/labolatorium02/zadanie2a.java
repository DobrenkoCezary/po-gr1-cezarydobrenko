package pl.edu.uwm.wmii.dobrenkocezary.labolatorium02;

import java.util.*;

public class zadanie2a {

    public static int ileNieparzystych(int tab[]){
        int a = 0;
        for (int i=0; i<tab.length; i++) if(tab[i]%2!=0) a++;
        return a;
    }

    public static int ileParzystych(int tab[]){
        int a = 0;
        for (int i=0; i<tab.length; i++) if(tab[i]%2==0) a++;
        return a;
    }

    public static void generuj (int tab[], int n, int min, int max){
        Random rand = new Random();
        for (int i = 0; i < n; i++)
            tab[i] = rand.nextInt(max - min +1) + min;
    }

    public static void main(String[] args) {
        Scanner InputValue = new Scanner(System.in);
        int n = InputValue.nextInt();
        int[] tab = new int[n];
        generuj(tab,n,-999,999);
        System.out.println(Arrays.toString(tab));
        if (n>=1 & n<=100){
            System.out.println("Ile nieparzystych: "+ileNieparzystych(tab));
            System.out.println("Ile parzystych: "+ileParzystych(tab));
        }

    }
}
