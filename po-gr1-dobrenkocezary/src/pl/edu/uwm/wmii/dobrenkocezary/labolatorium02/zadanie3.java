package pl.edu.uwm.wmii.dobrenkocezary.labolatorium02;

import java.util.Random;
import java.util.Scanner;

public class zadanie3 {

    public static void wypisz(int tab[][]){
        for(int i=0;i<tab.length;i++){
            for(int j=0;j<tab[i].length;j++)
                System.out.print(tab[i][j]+"\t");
            System.out.println();
        }
    }

    public static void generuj(int tab[][]){
        Random random = new Random();
        for(int i=0;i<tab.length;i++){
            for(int j=0;j<tab[i].length;j++)
                tab[i][j]=random.nextInt(20);
        }
    }

    public static void initialize(int macierzA[][], int macierzB[][]){
        generuj(macierzA);
        generuj(macierzB);
        wypisz(macierzA);
        System.out.println();
        wypisz(macierzB);
        System.out.println();
    }

    public static void main(String[] args) {
        Scanner InputValue = new Scanner(System.in);
        System.out.print("Podaj trzy liczby n,m,k z przedzialu od 1 do 10:");
        int m=InputValue.nextInt();
        int n=InputValue.nextInt();
        int k=InputValue.nextInt();
        int macierzA[][]=new int[m][n];
        int macierzB[][]=new int[n][k];
        int macierzC[][]=new int[m][k];

        initialize(macierzA,macierzB);

        for(int i=0;i<macierzA.length;i++) {
            for (int j = 0; j < macierzB[i].length; j++){
                int suma=0;
                for(int x=0;x<k;x++)
                    suma+=macierzA[i][x]*macierzB[x][j];
                macierzC[i][j]=suma;
            }
        }

        wypisz(macierzC);
    }
}
