package pl.edu.uwm.wmii.dobrenkocezary.labolatorium02;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class zadanie2g {

    public static void odwrocFragment(int tab[], int lewy, int prawy){
        for(int i = 0; i<=(prawy-lewy)/2; i++)
        {
            int temp = tab[lewy];
            tab[lewy] = tab[prawy];
            tab[prawy] = temp;
            lewy += 1;
            prawy -= 1;
        }
        System.out.println(Arrays.toString(tab));
    }

    public static void generuj (int tab[], int n, int min, int max){
        Random rand = new Random();
        for (int i = 0; i < n; i++)
            tab[i] = rand.nextInt(max - min +1) + min;
    }

    public static void main(String[] args) {
        Scanner InputValue = new Scanner(System.in);
        int n = InputValue.nextInt();
        int[] tab = new int[n];
        generuj(tab,n,-999,999);
        System.out.println(Arrays.toString(tab));
        if (n>=1 & n<=100) odwrocFragment(tab,1,3);
    }
}
