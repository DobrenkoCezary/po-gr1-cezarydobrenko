package pl.edu.uwm.wmii.dobrenkocezary.labolatorium02;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class zadanie2c {

    public static int ileMaksymalnych(int tab[]){
        int a = 0;
        int najwieksza = 0;
        for (int i = 0; i < tab.length; i++) if(tab[i]>a) najwieksza = tab[i];
        for (int i = 0; i < tab.length; i++) if (najwieksza == tab[i]) a++;
        return a;
    }

    public static void generuj (int tab[], int n, int min, int max){
        Random rand = new Random();
        for (int i = 0; i < n; i++)
            tab[i] = rand.nextInt(max - min +1) + min;
    }

    public static void main(String[] args) {
        Scanner InputValue = new Scanner(System.in);
        int n = InputValue.nextInt();
        int[] tab = new int[n];
        generuj(tab,n,-999,999);
        System.out.println(Arrays.toString(tab));
        if (n>=1 & n<=100) System.out.println("Ile maksymalnych: "+ileMaksymalnych(tab));

    }
}
