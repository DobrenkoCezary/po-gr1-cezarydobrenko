package pl.edu.uwm.wmii.dobrenkocezary.labolatorium09;

import java.time.*;
import java.util.Date;

public class Osoba implements Comparable<Osoba> {
    private String nazwisko;
    private LocalDate dataUrodzenia;

    Date date = new Date();
    Instant instant = date.toInstant();
    ZoneId defaultZoneId = ZoneId.systemDefault();
    LocalDate localDate = instant.atZone(defaultZoneId).toLocalDate();

    Osoba(String nazwisko, LocalDate dataur){
        this.nazwisko = nazwisko;
        this.dataUrodzenia = dataur;
    }

    public int compareTo(Osoba target){
        if((this.nazwisko.compareTo(target.nazwisko)) == 0) {
            if ((this.dataUrodzenia.compareTo(target.dataUrodzenia)) == 0) {
                return 0;
            } else return this.dataUrodzenia.compareTo(target.dataUrodzenia);
        } else return this.nazwisko.compareTo(target.nazwisko);
    }


    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        Osoba other = (Osoba) obj;
        if (nazwisko == null) if (other.nazwisko != null) return false;
        else if (!nazwisko.equals(other.nazwisko)) return false;
        if (this.dataUrodzenia != other.dataUrodzenia) return false;
        return true;
    }

    public String toString(){
        String result = "";
        result += getClass().getName();
        result += (" [" + this.nazwisko + "][" + dataUrodzenia.toString() + "]");
        return result;
    }

    LocalDate today = LocalDate.now();

    public int ileLat(){
        Period p = Period.between(dataUrodzenia, today);
        return p.getYears();
    }

    public int ileMiesiecy(){
        Period p = Period.between(dataUrodzenia, today);
        return p.getMonths();
    }

    public int ileDni(){
        Period p = Period.between(dataUrodzenia, today);
        return p.getDays();
    }

}