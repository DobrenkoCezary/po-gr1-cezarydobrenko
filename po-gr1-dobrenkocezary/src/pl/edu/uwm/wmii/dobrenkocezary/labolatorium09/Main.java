package pl.edu.uwm.wmii.dobrenkocezary.labolatorium09;

import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        Osoba o = new Osoba("Bonzo", LocalDate.of(1998, Month.MARCH, 13));
        Osoba o2 = new Osoba("Haker", LocalDate.of(1998, Month.MARCH, 13));

        int x = o2.ileLat();
        int y = o2.ileMiesiecy();
        int z = o2.ileDni();
        System.out.println("-----------");
        System.out.println("Lat: "+x + " Miesiecy: " +  Math.abs(y) + " Dni: " + Math.abs(z));
        System.out.println("-----------");
        System.out.println(o.equals(o2));
        System.out.println(o.toString());
        System.out.println(o.compareTo(o2));
        test();
    }

    public static void test(){
        Osoba[] grupa = new Osoba[5];

        grupa[0] = new Osoba("Grzelecki", LocalDate.of(1998,03,13));
        grupa[1] = new Osoba("Lis", LocalDate.of(1979,11,20));
        grupa[4] = new Osoba("Kowalski", LocalDate.of(1994,03,13));
        grupa[3] = new Osoba("Kowalski", LocalDate.of(1992,01,23));
        grupa[2] = new Osoba("Lis", LocalDate.of(1980,11,20));


        for (int i = 0; i < grupa.length; i++){
            System.out.println(grupa[i].toString() + "\t\tLat: " + grupa[i].ileLat() + ", Miesiecy: " + Math.abs(grupa[i].ileMiesiecy()) + ", Dni: " +  Math.abs(grupa[i].ileDni()));
        }
        Arrays.sort(grupa);
        System.out.println("===============================================");

        for (int i = 0; i < grupa.length; i++){
            System.out.println(grupa[i].toString() + "\t\tLat: " + grupa[i].ileLat() + ", Miesiecy: " + Math.abs(grupa[i].ileMiesiecy()) + ", Dni: " + Math.abs(grupa[i].ileDni()));
        }
    }
}
