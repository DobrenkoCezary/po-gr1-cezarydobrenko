package pl.imiajd.dobrenko;


public class Nauczyciel extends Osoba
{
    public Nauczyciel(String nazwisko, int rokUrodzenia, double pensja){
        super(nazwisko, rokUrodzenia);
        this.pensja = pensja;
    }

    public double getpensja(){
        return pensja;
    }

    public void setpensja(double pensja){
        this.pensja = pensja;
    }

    public String toString(){
        return "kierunek: " + getpensja() + " nazwisko: " + getnazwisko() + " Rok Urodzenia: "+ getRokUrodzenia();
    }


    private double pensja;
}
