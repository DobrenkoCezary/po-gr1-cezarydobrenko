package pl.imiajd.dobrenko;
import java.time.LocalDate;

public abstract class Instrument {
    public Instrument() {
        this.producent = "anonim";
        this.rokProdukcji = LocalDate.of(1970, 1, 1);
    }
    public Instrument(String producent, LocalDate rokProdukcji) {
        this.producent = producent;
        this.rokProdukcji = rokProdukcji;
    }
    public String getProducent() { return this.producent; }
    public LocalDate getRokProdukcji() { return this.rokProdukcji; }
    public abstract String dzwiek();
    @Override
    public boolean equals(Object otherObject) {
        if (this == otherObject)
            return true;
        if (otherObject == null)
            return false;
        if (!(otherObject instanceof Instrument))
            return false;
        Instrument other = (Instrument) otherObject;

        return this.getProducent() == other.getProducent() && this.getRokProdukcji() == other.getRokProdukcji();
    }
    @Override
    public String toString() {
        return String.format("%s: Instrument stworzony przez %s w roku %d", this.getClass().getSimpleName(), this.getProducent(), this.getRokProdukcji().getYear());
    }

    private String producent;
    private LocalDate rokProdukcji;
}
