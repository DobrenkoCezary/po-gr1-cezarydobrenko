package pl.imiajd.dobrenko;


public class Osoba
{
    public Osoba(String nazwisko, int rokUrodzenia){
        this.nazwisko = nazwisko;
        this.rokUrodzenia = rokUrodzenia;
    }

    public String getnazwisko(){
        return nazwisko;
    }

    public void setnazwisko(String nazwisko){
        this.nazwisko = nazwisko;
    }

    public int getRokUrodzenia(){
        return rokUrodzenia;
    }

    public void setRokUrodzenia(int rokUrodzenia){
        this.rokUrodzenia = rokUrodzenia;
    }



    private String nazwisko;
    private int rokUrodzenia;

}
