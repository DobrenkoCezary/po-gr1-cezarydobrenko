package pl.imiajd.dobrenko;

import java.util.Optional;
import java.util.OptionalInt;

public class Adres {

    public Adres(String ulica, int numer_domu, int numer_mieszkania, String miasto, String kod_pocztowy){
        this.ulica = ulica;
        this.numer_domu = numer_domu;
        this.numer_mieszkania = numer_mieszkania;
        this.miasto = miasto;
        this.kod_pocztowy = kod_pocztowy;
    }

    public Adres(String ulica, int numer_domu, String miasto, String kod_pocztowy){
        this.ulica = ulica;
        this.numer_domu = numer_domu;
        this.miasto = miasto;
        this.kod_pocztowy = kod_pocztowy;
    }

    private String ulica;
    private int numer_domu;
    private int numer_mieszkania;
    private String miasto;
    private String kod_pocztowy;


    public void pokaz(){
        if(this.numer_mieszkania != 0){
            System.out.println(kod_pocztowy + " " + miasto);
            System.out.println(ulica + " " + numer_domu + "/" + numer_mieszkania);
        } else {
            System.out.println(kod_pocztowy + " " + miasto);
            System.out.println(ulica + " " + numer_domu);
        }
    }



}

