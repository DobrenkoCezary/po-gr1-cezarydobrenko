package pl.imiajd.dobrenko;

import java.time.LocalDate;

public class Skrzypce extends Instrument {
    public Skrzypce() { super(); }
    public Skrzypce(String producent, LocalDate rokProdukcji) { super(producent, rokProdukcji); }
    public String dzwiek() {
        return "dźwięk skrzypiec";
    }
}
