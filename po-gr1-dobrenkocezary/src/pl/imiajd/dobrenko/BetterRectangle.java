package pl.imiajd.dobrenko;

import java.awt.*;

public class BetterRectangle extends Rectangle
{
    public BetterRectangle(int height, int width){
        super(height, width);
    }

    public double getPerimeter(){
        return (2*this.height) + (2*this.width);
    }

    public double getArea(){
        return this.height * this.width;
    }

    public void setheight(int height){
        this.height = height;
    }

    public void setwidth(int width){
        this.width = width;
    }


}
