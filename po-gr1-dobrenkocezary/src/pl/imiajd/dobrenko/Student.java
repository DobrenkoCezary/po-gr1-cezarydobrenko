package pl.imiajd.dobrenko;


public class Student extends Osoba
{
    public Student(String nazwisko, int rokUrodzenia, String kierunek){
        super(nazwisko, rokUrodzenia);
        this.kierunek = kierunek;
    }

    public String getkierunek(){
        return kierunek;
    }

    public void setkierunek(String kierunek){
        this.kierunek = kierunek;
    }

    public String toString(){
        return "kierunek: " + getkierunek() + " nazwisko: " + getnazwisko() + " Rok Urodzenia: "+ getRokUrodzenia();
    }


    private String kierunek;

}
